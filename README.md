# Layout básico
## Enunciado
1. Crea un directorio para iniciar un nuevo proyecto HTML con dos archivos: index.html y estilos.css
2. En el archivo index.html escribe el esqueleto inicial de HTML.
3. En el archivo index.html tienes que cargar dos hojas de estilos CSS: la de Bootstrap y tu archivo estilos.css.
4. Copia este código HTML dentro del <body> del archivo index.html:  
```html
 <div>
  <header>
    <h1>Título</h1>
  </header>
  <section class="contenidos">
    <main>
      <h2>Titular de la noticia</h2>
      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sapiente officia quo numquam quisquam facere ab nemo natus, explicabo atque corrupti consectetur, labore perspiciatis tempore expedita minima modi, quidem culpa qui?</p>
      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sapiente officia quo numquam quisquam facere ab nemo natus, explicabo atque corrupti consectetur, labore perspiciatis tempore expedita minima modi, quidem culpa qui?</p>
    </main>
    <aside>
      <ul>
        <li>Noticias de deporte</li>
        <li>Noticias de ocio</li>
        <li>Noticias de política</li>
        <li>Noticias de espectáculos</li>
      </ul>
    </aside>
  </section>
  <footer>
    Texto del footer
  </footer>
</div>
```
4. Copia este código en el archivo estilos.css:

```css
body {
  color: #fff;
}
header, footer {
  height: 100px;
  background-color: #2d3142;
}
.contenidos {
  min-height: 600px;
}
main {
  background-color: #d8d5db;
}
aside {
  background-color: #adacb5;
}
main, aside {
  padding-top: 20px;
  padding-bottom: 20px;  
  color: #333;
}
```

5. Añade al código HTML las clases necesarias para conseguir el siguiente layout. No tienes que tocar nada del archivo estilos.css.

![layout](https://bitbucket.org/bootstrap-madrid/layout-basico/raw/7812defa6c707ebf38d94792c371c8ab14b63bc9/layout.png "layout")